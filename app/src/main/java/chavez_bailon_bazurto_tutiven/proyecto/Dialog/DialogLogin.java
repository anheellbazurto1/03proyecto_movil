package chavez_bailon_bazurto_tutiven.proyecto.Dialog;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import chavez_bailon_bazurto_tutiven.proyecto.R;

public class DialogLogin extends android.app.Dialog implements View.OnClickListener {
    private final Context a;
    private OnMyDialogResult mDialogResult;

    public DialogLogin(Activity context) {
        super(context, R.style.Theme_AppCompat_DayNight_Dialog_Alert);
        this.a = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialogologin);

        Button iniciar = (Button) findViewById(R.id.iniciar);
        Button cancealar = (Button) findViewById(R.id.cancealar);
        iniciar.setOnClickListener(this);
        cancealar.setOnClickListener(this);

    }

    public void setDialogResult(OnMyDialogResult dialogResult) {
        mDialogResult = dialogResult;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iniciar:
                if (mDialogResult != null) {
                    mDialogResult.finish(String.valueOf(true), String.valueOf(true), String.valueOf(true));
                }
                dismiss();
                break;
            case R.id.cancealar:
                dismiss();
                break;
            default:
                break;
        }
        dismiss();

    }

    public interface OnMyDialogResult {
        void finish(String result, String result2, String rescult3);
    }
}
