package chavez_bailon_bazurto_tutiven.proyecto;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import chavez_bailon_bazurto_tutiven.proyecto.Fragment.Historial;
import chavez_bailon_bazurto_tutiven.proyecto.Fragment.Inicio;
import chavez_bailon_bazurto_tutiven.proyecto.Fragment.Perfil;
import chavez_bailon_bazurto_tutiven.proyecto.UI.SectionsPageAdapter;

public class Home extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);

        ViewPager viewPager = findViewById(R.id.view_pager);
        TabLayout tabs = findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);

        SectionsPageAdapter adapter = new SectionsPageAdapter(getSupportFragmentManager());

        adapter.addFragment(new Inicio(), "Inicio");
        adapter.addFragment(new Perfil(), "Perfil");
        adapter.addFragment(new Historial(), "Historial");

        viewPager.setAdapter(adapter);




    }
}