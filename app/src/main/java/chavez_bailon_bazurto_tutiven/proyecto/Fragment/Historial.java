package chavez_bailon_bazurto_tutiven.proyecto.Fragment;


import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

import chavez_bailon_bazurto_tutiven.proyecto.Adapter.ProductoAdapter;
import chavez_bailon_bazurto_tutiven.proyecto.Class.Producto;
import chavez_bailon_bazurto_tutiven.proyecto.Dialog.DialogLogin;
import chavez_bailon_bazurto_tutiven.proyecto.R;

public class Historial extends Fragment {
    private ListView listView;
    private ProductoAdapter mAdapter;
    private boolean session;
    private View idsessionoff;
    private View idsessionooo;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.f_historial, container, false);



        session=true;
        idsessionoff=view.findViewById(R.id.idsessionoff);
        idsessionooo=view.findViewById(R.id.idsessionon);

        Button btniniciar=view.findViewById(R.id.btniniciar);

        btniniciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogLogin cd =new DialogLogin(getActivity());
                cd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                cd.setCancelable(false);
                cd.show();
                cd.setDialogResult(new DialogLogin.OnMyDialogResult(){
                    public void finish(String result,String result2,String result3){
                        // now you can use the 'result' on your activity
                        session= Boolean.parseBoolean(result);
                        Log.e("as",result);
                        if(!session){

                            idsessionoff.setVisibility(View.VISIBLE);
                            idsessionooo.setVisibility(View.GONE);


                        }else{
                            idsessionoff.setVisibility(View.GONE);
                            idsessionooo.setVisibility(View.VISIBLE);
                        }

                    }
                });
            }
        });

        if(session){

            idsessionoff.setVisibility(View.VISIBLE);
            idsessionooo.setVisibility(View.GONE);

        }else{
            idsessionoff.setVisibility(View.GONE);
            idsessionooo.setVisibility(View.VISIBLE);
            listView = (ListView) view.findViewById(R.id.listhistorial);
            ArrayList<Producto> moviesList = new ArrayList<>();
            moviesList.add(new Producto(R.drawable.cevichee, "After Earth: Comprado" , 2013,5,0.50));

            mAdapter = new ProductoAdapter(getContext(),moviesList);
            listView.setAdapter(mAdapter);
        }
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}