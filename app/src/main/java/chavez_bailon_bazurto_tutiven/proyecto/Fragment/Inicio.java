package chavez_bailon_bazurto_tutiven.proyecto.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

import chavez_bailon_bazurto_tutiven.proyecto.Adapter.ProductoAdapter;
import chavez_bailon_bazurto_tutiven.proyecto.Class.Producto;
import chavez_bailon_bazurto_tutiven.proyecto.R;

public class Inicio extends Fragment {
    private ListView listView;
    private ProductoAdapter mAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.f_inicio, container, false);


        listView = (ListView) view.findViewById(R.id.listaproductos);
        ArrayList<Producto> moviesList = new ArrayList<>();
        moviesList.add(new Producto(R.drawable.cevichee, "Ceviche" , 0,0,1.50));
        moviesList.add(new Producto(R.drawable.chifle, "Chifle" , 0,0,1.25));

        moviesList.add(new Producto(R.drawable.corviche, "Corviche" , 0,0,0.50));
        moviesList.add(new Producto(R.drawable.empanada, "Empanada" , 0,0,1.00));


        mAdapter = new ProductoAdapter(getContext(),moviesList);
        listView.setAdapter(mAdapter);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
