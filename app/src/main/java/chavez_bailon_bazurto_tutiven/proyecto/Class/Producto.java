package chavez_bailon_bazurto_tutiven.proyecto.Class;

public class Producto  {
    private String pname;
    private int pImageDrawable;
    private int pcantidad;
    private int pnmesa;
    private double pprecio;

    public Producto( int pImageDrawable,String pname, int pcantidad, int pnmesa, double pprecio) {
        this.pname = pname;
        this.pImageDrawable = pImageDrawable;
        this.pcantidad = pcantidad;
        this.pnmesa = pnmesa;
        this.pprecio = pprecio;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public int getpImageDrawable() {
        return pImageDrawable;
    }

    public void setpImageDrawable(int pImageDrawable) {
        this.pImageDrawable = pImageDrawable;
    }

    public int getPcantidad() {
        return pcantidad;
    }

    public void setPcantidad(int pcantidad) {
        this.pcantidad = pcantidad;
    }

    public int getPnmesa() {
        return pnmesa;
    }

    public void setPnmesa(int pnmesa) {
        this.pnmesa = pnmesa;
    }

    public double getPprecio() {
        return pprecio;
    }

    public void setPprecio(double pprecio) {
        this.pprecio = pprecio;
    }
}

