package chavez_bailon_bazurto_tutiven.proyecto.Adapter;

import android.content.Context;
import android.graphics.Movie;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import chavez_bailon_bazurto_tutiven.proyecto.Class.Producto;
import chavez_bailon_bazurto_tutiven.proyecto.R;

public class ProductoAdapter extends ArrayAdapter<Producto> {
    private Context mContext;
    private List<Producto> moviesList = new ArrayList<>();

    public ProductoAdapter(@NonNull Context context, ArrayList<Producto> list) {
        super(context, 0 , list);
        mContext = context;
        moviesList = list;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if(listItem == null)
            listItem = LayoutInflater.from(mContext).inflate(R.layout.list_item,parent,false);

        Producto currentMovie = moviesList.get(position);

        ImageView image = (ImageView)listItem.findViewById(R.id.imageView);
        image.setImageResource(currentMovie.getpImageDrawable());

        TextView name = (TextView) listItem.findViewById(R.id.name);
        name.setText(currentMovie.getPname());

        TextView precio = (TextView) listItem.findViewById(R.id.precio);
        precio.setText("$ "+currentMovie.getPprecio());

        return listItem;
    }

}
